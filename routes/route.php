<?php
/**
 * Created by PhpStorm.
 * User: win10
 * Date: 2018/5/16
 * Time: 17:09
 */

Route::group([
    'prefix'=>'x/dingtalk',
    'middleware' => ['web'],
    'as' => 'x.dingtalk.',
    'namespace'=>'Xbhub\Dingtalk\Http\Controllers'
], function(){

    Route::post('/recevie', 'CallbackController@recevie')->name('recevie');
});