<?php

/*
 * This file is part of the xbhub/dingtalk.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Dingtalk\Api\Checkin;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * @param $departmentId
     * @param $start
     * @param $end
     * @param array $params
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function record($departmentId, $start, array $params = [])
    {
        return $this->httpGet('checkin/record', [
            'department_id' => $departmentId,
            'start_time'    => $start
        ] + $params);
    }
}
