<?php

/*
 * This file is part of the xbhub/dingtalk.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Dingtalk\Api\User;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * [code description]
     * @return [type] [description]
     */
    public function code(string $code)
    {
        return $this->httpGet('user/getuserinfo', ['code' => $code]);
    }

    /**
     * @param string $userId
     *
     * @return array
     */
    public function get(string $userId)
    {
        return $this->httpGet('user/get', ['userid' => $userId]);
    }

    /**
     * Create a new user.
     *
     * @param array $params
     *
     * @return array
     */
    public function create(array $params)
    {
        return $this->httpPostJson('user/create', $params);
    }

    /**
     * Update an exist user.
     *
     * @param array $params
     *
     * @return array
     */
    public function update(array $params)
    {
        return $this->httpPostJson('user/update', $params);
    }

    /**
     * @param array|string $userId
     *
     * @return array
     */
    public function delete($userId)
    {
        if (is_array($userId)) {
            return $this->http->json('user/batchdelete', ['useridlist' => $userId]);
        }

        return $this->httpGet('user/delete', ['userid' => $userId]);
    }

    /**
     * @param int   $departmentId
     * @param array $params
     *
     * @return array
     */
    public function simpleList(int $departmentId, array $params = [])
    {
        return $this->httpGet('user/simplelist', [
            'department_id' => $departmentId,
        ] + $params);
    }

    /**
     * @param int   $departmentId
     * @param int   $size
     * @param int   $offset
     * @param array $params
     *
     * @return array
     */
    function list(int $departmentId, $size = 100, $offset = 0, array $params = []) {
        return $this->httpGet('user/list', [
            'department_id' => $departmentId,
            'offset'        => $offset,
            'size'          => $size,
        ] + $params);
    }

    /**
     * @return array
     */
    public function admin()
    {
        return $this->httpGet('user/get_admin');
    }

    /**
     * UnionId to userId.
     *
     * @param string $unionId
     *
     * @return array
     */
    public function toUserId(string $unionId)
    {
        return $this->httpGet('user/getUseridByUnionid', [
            'unionid' => $unionId,
        ]);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function count(array $params)
    {
        return $this->httpGet('user/get_org_user_count', $params);
    }
}
