<?php

/*
 * This file is part of the xbhub/dingtalk.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Dingtalk\Api;

use Pimple\Container;

/**
 * Class Application.
 *
 * @author jory <jorycn@163.com>
 *
 * @property \namespace Xbhub\Dingtalk\Api\Auth\Client $auth
 * @property \namespace Xbhub\Dingtalk\Api\Chat\Client $chat
 * @property \namespace Xbhub\Dingtalk\Api\Role\Client $role
 * @property \namespace Xbhub\Dingtalk\Api\User\Client $user
 * @property \namespace Xbhub\Dingtalk\Api\Media\Client $media
 * @property \namespace Xbhub\Dingtalk\Api\Jssdk\Client $jssdk
 * @property \namespace Xbhub\Dingtalk\Api\Checkin\Client $checkin
 * @property \namespace Xbhub\Dingtalk\Api\Message\Client $message
 * @property \namespace Xbhub\Dingtalk\Api\Attendance\Client $attendance
 * @property \namespace Xbhub\Dingtalk\Api\Kernel\Credential $credential
 * @property \namespace Xbhub\Dingtalk\Api\Department\Client $department
 * @property \namespace Xbhub\Dingtalk\Api\Message\AsyncClient $async_message
 */
class Application extends Container
{
    /**
     * @var array
     */
    protected $providers = [
        Auth\ServiceProvider::class, //授权
        Chat\ServiceProvider::class, //会话消息
        Role\ServiceProvider::class, // 角色
        User\ServiceProvider::class, // 人员
        Jssdk\ServiceProvider::class, //jssdk
        Media\ServiceProvider::class, // 附件
        Kernel\ServiceProvider::class, // 核心
        Checkin\ServiceProvider::class, // 签到
        Message\ServiceProvider::class, // 企业消息
        Attendance\ServiceProvider::class, // 考勤数据
        Department\ServiceProvider::class, // 部门
        Sns\ServiceProvider::class, //
        Smartwork\ServiceProvider::class, // 智能办公
        Callback\ServiceProvider::class, // 事件回调
        Process\ServiceProvider::class, // 审批
        Cspace\ServiceProvider::class, // 钉盘
        Robot\ServiceProvider::class, //Robot
        Workrecord\ServiceProvider::class, //Robot
        Calendar\ServiceProvider::class, //Calendar
        Alitrip\ServiceProvider::class, // 阿里商旅
    ];

    /**
     * Application constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct();

        $this['config'] = function () use ($config) {
            return new Kernel\Config($config);
        };

        $this->registerProviders();
    }

    /**
     * Register providers.
     */
    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            $this->register(new $provider());
        }
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }
}
