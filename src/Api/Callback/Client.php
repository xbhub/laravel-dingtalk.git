<?php


namespace Xbhub\Dingtalk\Api\Callback;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Messages\Message;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{
    /**
     * @param array $call_back_tag
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function register_call_back(array $call_back_tag)
    {
        $_config = config('dingtalk');
        return $this->httpPostJson('call_back/register_call_back', [
            'call_back_tag' => $call_back_tag,
            'token' => $_config['token'],
            'aes_key' => $_config['aes_key'],
            'url' => route('x.dingtalk.recevie')
        ]);
    }

    /**
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function get_call_back()
    {
        return $this->httpGet('call_back/get_call_back');
    }

    /**
     * @param array $call_back_tag
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function update_call_back(array $call_back_tag)
    {
        $_config = config('dingtalk');
        return $this->httpPostJson('call_back/update_call_back', [
            'call_back_tag' => $call_back_tag,
            'token' => $_config['token'],
            'aes_key' => $_config['aes_key'],
            'url' => route('x.dingtalk.recevie')
        ]);
    }
}
