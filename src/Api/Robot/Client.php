<?php


namespace Xbhub\Dingtalk\Api\Robot;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Exceptions\ClientError;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    public function send($webhook, $type = 'text', $content = '', $at = [])
    {
        $res = $this->app['http_client']->post($webhook, [
            'json' => [
                'msgtype' =>$type,
                $type =>$content,
                'at' => $at
            ]
        ]);
        return json_decode($res->getBody()->getContents(), true);
    }

}
