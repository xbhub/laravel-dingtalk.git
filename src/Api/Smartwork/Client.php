<?php

/*
 * This file is part of the xbhub/dingtalk.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Dingtalk\Api\Smartwork;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\MakesHttpRequests;
use Illuminate\Support\Arr;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{
    use MakesHttpRequests;

    public function blackboard($userid)
    {
        $result = $this->httpPostMethod('dingtalk.smartwork.blackboard.listtopten', [
            'userid'=> $userid
        ]);
        return array_reverse(Arr::sort($result['result']['blackboard_list'], 'gmt_create'));
    }

}
