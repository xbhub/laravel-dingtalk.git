<?php

/*
 * This file is part of the xbhub/dingtalk.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Dingtalk\Api\Kernel;

use Xbhub\Dingtalk\Api\Application;

/**
 * Class Credential.
 *
 * @author jory <jorycn@163.com>
 */
class Credential
{
    use MakesHttpRequests;

    /**
     * @var \namespace Xbhub\Dingtalk\Api\Application
     */
    protected $app;

    /**
     * Credential constructor.
     *
     * @param \namespace Xbhub\Dingtalk\Api\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get credential token.
     *
     * @return string
     */
    public function token()
    {
        if ($value = $this->app['cache']->get($this->cacheKey())) {
            return $value;
        }

        $result = $this->request('GET', 'gettoken', [
            'query' => $this->credentials(),
        ]);

        $this->setToken($token = $result['access_token'], 7000);

        return $token;
    }

    /**
     * @param string                 $token
     * @param int|\DateInterval|null $ttl
     *
     * @return $this
     */
    public function setToken($token, $ttl = null)
    {
        $this->app['cache']->set($this->cacheKey(), $token, $ttl);

        return $this;
    }

    /**
     * @return array
     */
    protected function credentials(): array
    {
        return [
            'appkey'     => $this->app['config']->get('app_id'),
            'appsecret' => $this->app['config']->get('app_secret'),
        ];
    }

    /**
     * @return string
     */
    protected function cacheKey(): string
    {
        return 'dingtalk.access_token.' . md5(json_encode($this->credentials()));
    }
}
