<?php


namespace Xbhub\Dingtalk\Api\Workrecord;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Exceptions\ClientError;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * Undocumented function
     *
     * @param [type] $userid
     * @param [type] $title
     * @param [type] $url
     * @param [type] $data ['title','content']
     * @return void
     */
    public function add($userid, $title, $url, $data)
    {
        return $this->httpPostJson('topapi/workrecord/add', [
            'userid' => $userid,
            'create_time' => time()*1000,
            'title' => $title,
            'url' => $url,
            'formItemList' => $data
        ]);
    }

    /**
     * Undocumented function
     *
     * @param [type] $userid
     * @param [type] $record_id
     * @return void
     */
    public function update($userid, $record_id)
    {
        return $this->httpPostJson('topapi/workrecord/update', [
            'userid' => $userid,
            'record_id' => $record_id
        ]);
    }

}
