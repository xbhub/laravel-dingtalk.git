<?php


namespace Xbhub\Dingtalk\Api\Process;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Exceptions\ClientError;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{
    /**
     * @param $userid
     * @param int $offset
     * @param int $size
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function listbyuserid($userid = '', $offset = 0, $size = 100)
    {
        return $this->httpPostJson('topapi/process/listbyuserid', [
            'userid' => $userid,
            'offset' => $offset,
            'size' => $size
        ]);
    }

    /**
     * @param $process_instance_id
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function get($process_instance_id)
    {
        return $this->httpGet('topapi/processinstance/get', [
            'process_instance_id' => $process_instance_id
        ]);
    }


    /**
     * @param $process_code
     * @param $originator_user_id
     * @param $dept_id
     * @param $approvers
     * @param $form_component_values
     * @param array $option
     * @return mixed
     * @throws ClientError
     */
    public function create($process_code, $originator_user_id, $dept_id, $approvers, $form_component_values, $option = [])
    {
        $res = $this->app['http_client']->post('topapi/processinstance/create', [
            'form_params' => [
                'access_token' => $this->app['credential']->token(),
                'process_code' => $process_code,
                'originator_user_id' => $originator_user_id,
                'dept_id' => $dept_id,
                'approvers' => $approvers,
                'cc_list' => isset($option['cc_list'])?implode(array_unique(explode(',', $option['cc_list'])), ','):'',
                'cc_position' => isset($option['cc_position'])?$option['cc_position']:'FINISH',
                'form_component_values' => json_encode($form_component_values)
            ]
        ]);

        $result = json_decode($res->getBody()->getContents(), true);
        // $res =  $this->httpPostMethod('dingtalk.smartwork.bpms.processinstance.create', $data);
        // if(!$res['result']['is_success']) {
        //     throw new ClientError($res['result']['error_msg'], $res['result']['ding_open_errcode']);
        // }
        return $result;
    }

}
