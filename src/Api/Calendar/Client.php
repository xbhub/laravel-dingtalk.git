<?php


namespace Xbhub\Dingtalk\Api\Calendar;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Exceptions\ClientError;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * Undocumented function
     *
     * @param [type] $userid
     * @param [type] $title
     * @param [type] $url
     * @param [type] $calendar_type 日程类型:task-任务;meeting-会议;notification-提醒
     * @return void
     */
    public function create($title, $id, $summary, $url, $create_by, $calendar_type, $location, $userids, $start_at, $end_at,$description)
    {
        return $this->httpPostJson('topapi/calendar/create', [
            'create_vo' => [
                'summary' => $summary,
                'reminder' => ['minutes' => 5],
                'remind_type' => 'app',
                'location' => $location,
                'creator_userid' => $create_by,
                'uuid' => \Ramsey\Uuid\Uuid::uuid4(),
                'biz_id' => $id,
                'source' => [
                    'title' => $title,
                    'url' => $url,
                    'description' =>$description
                ],
                'receiver_userids' => $userids,
                'start_time' => ['unix_timestamp' => $start_at],
                'end_time' => ['unix_timestamp' => $end_at],
                'calendar_type' => $calendar_type
            ]
        ]);
    }


}
